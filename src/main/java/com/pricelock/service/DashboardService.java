package com.pricelock.service;

import com.pricelock.model.OrderResponse;

import java.util.List;

public interface DashboardService {
    List<OrderResponse> retrieveAllOrder(Long id);
}
