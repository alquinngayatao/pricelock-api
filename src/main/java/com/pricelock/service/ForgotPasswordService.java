package com.pricelock.service;

public interface ForgotPasswordService {
    void forgotPasswordStoreOwner(String emailAddress);
}
