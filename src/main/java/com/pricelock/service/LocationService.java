package com.pricelock.service;

import com.pricelock.model.BarangayRequest;
import com.pricelock.model.CityRequest;
import com.pricelock.model.Location;

import java.util.List;

public interface LocationService {
    List<Location> getAllProvince();
    List<Location> getAllCityByProvince(CityRequest cityRequest);
    List<Location> getAllBarangayByCity(BarangayRequest barangayRequest);
}
