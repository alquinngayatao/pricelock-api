package com.pricelock.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface JwtUserDetails {
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
