package com.pricelock.service;

import com.pricelock.model.ResetPasswordRequest;

public interface ResetPasswordService {
    void resetPassword(ResetPasswordRequest resetPasswordRequest);
}
