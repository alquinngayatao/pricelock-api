package com.pricelock.service;

import com.pricelock.model.AdminRegisterRequest;
import com.pricelock.model.CustomerRegistrationRequest;
import com.pricelock.model.StoreOwnerRegisterRequest;

public interface RegistrationService {
    void registerAdmin(AdminRegisterRequest adminRegisterRequest);
    void registerStoreOwner(StoreOwnerRegisterRequest storeOwnerRegisterRequest);
    void registerCustomer(CustomerRegistrationRequest customerRegistrationRequest);
    void confirmRegistration(Long id);
}
