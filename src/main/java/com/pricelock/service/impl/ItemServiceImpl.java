package com.pricelock.service.impl;

import com.pricelock.constants.CommonConstants;
import com.pricelock.exception.APIException;
import com.pricelock.model.AddItemRequest;
import com.pricelock.model.AddReviewRequest;
import com.pricelock.model.CheckoutRequest;
import com.pricelock.model.ItemDetail;
import com.pricelock.model.OrderRequest;
import com.pricelock.model.ReviewResponse;
import com.pricelock.model.StoreResponse;
import com.pricelock.repository.CustomerOrderRepository;
import com.pricelock.repository.ItemOrderRepository;
import com.pricelock.repository.ItemRepository;
import com.pricelock.repository.ReviewRepository;
import com.pricelock.repository.UserRepository;
import com.pricelock.service.ItemService;
import com.pricelock.table.CustomerOrder;
import com.pricelock.table.Item;
import com.pricelock.table.ItemOrder;
import com.pricelock.table.Review;
import com.pricelock.table.User;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.sql.rowset.serial.SerialBlob;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {
    private final ItemRepository itemRepository;
    private final ItemOrderRepository itemOrderRepository;
    private final ReviewRepository reviewRepository;
    private final UserRepository userRepository;
    private final CustomerOrderRepository customerOrderRepository;

    @Override
    public void addItem(AddItemRequest addItemRequest) {
        try {
            log.info("Adding item");
            itemRepository.save(Item.builder()
                    .description(addItemRequest.getDescription())
                    .name(addItemRequest.getName())
                    .price(addItemRequest.getPrice())
                    .image(convertToBlob(addItemRequest.getImage()))
                    .storeOwnerId(addItemRequest.getId())
                    .build());
            log.info("item added");
        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    @Override
    public void addReview(AddReviewRequest addReviewRequest) {
        try {
            log.info("Adding review");
            reviewRepository.save(Review.builder()
                    .itemId(addReviewRequest.getItemId())
                    .name(addReviewRequest.getName())
                    .feedback(addReviewRequest.getFeedback())
                    .rate(addReviewRequest.getRate())
                    .build());
            log.info("Review added");
        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    @Override
    public List<ReviewResponse> retrieveAllReview(Long itemId) {
        try {
            log.info("Retrieving reviews");
            List<Review> reviews = reviewRepository.findAllReviewById(itemId);
            Review reviewMapper = new Review();
            log.info("Reviews retrieved");
            return reviewMapper.reviewToResponseAll(reviews);
        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    @Override
    public void order(OrderRequest orderRequest) {
        try {
            log.info("Order item");
            Set<CustomerOrder> customerOrders = new HashSet<>();
            if (Objects.nonNull(orderRequest)) {
                ItemOrder itemOrder = itemOrderRepository.save(ItemOrder.builder()
                        .ready(Boolean.FALSE)
                        .delivered(Boolean.FALSE)
                        .customerId(orderRequest.getCustomerId())
                        .build());
                Long storeOwnerId;
                for (ItemDetail itemDetail : orderRequest.getItemDetails()) {
                    Item item = itemRepository.findById(itemDetail.getId()).orElse(null);
                    if (Objects.nonNull(item)) {
                        customerOrders.add(CustomerOrder.builder()
                                .item(item)
                                .quantity(itemDetail.getQuantity())
                                .itemOrder(itemOrder)
                                .build());
                    }
                }

               if(!Collections.isEmpty(customerOrders)) {
                   customerOrderRepository.saveAll(customerOrders);
               }
//                itemOrder = itemOrder.toBuilder()
//                        .customerOrders(customerOrders)
//                        .build();
//                itemOrderRepository.save(itemOrder);
            }
            log.info("Order Requested");
        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    @Override
    public void checkout(CheckoutRequest checkoutRequest) {
        try {
            log.info("Checking out");
            ItemOrder itemOrder =
                    itemOrderRepository.findItemOrderByCustomerId(checkoutRequest.getId()).orElse(null);
            if (Objects.nonNull(itemOrder)) {
                itemOrder.setDelivered(Boolean.TRUE);
                itemOrderRepository.save(itemOrder);
            }
            log.info("Checked out");
        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    @Override
    public List<StoreResponse> retrieveAllStore() {
        try {
            log.info("Retrieve all stores");
            List<StoreResponse> storeResponses = new ArrayList<>();
            List<User> users = userRepository.findAllUserByRole(CommonConstants.STORE_OWNER);
            Item itemMapper = new Item();
            for (User user : users) {
                if (Objects.nonNull(user.getUserInfo())) {
                    storeResponses.add(StoreResponse.builder()
                            .storeName(user.getUserInfo().getStoreName())
                            .items(itemMapper.itemAllToResponse(itemRepository.findAllItemByStoreOwnerId(user.getId())))
                            .storeId(user.getId())
                            .build());
                }
            }
            log.info("Done retrieving");
            return storeResponses;
        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    private Blob convertToBlob(String base64) throws SQLException {
        return new SerialBlob(base64.getBytes(StandardCharsets.UTF_8));
    }
}
