package com.pricelock.service.impl;

import com.pricelock.constants.CommonConstants;
import com.pricelock.exception.APIException;
import com.pricelock.model.AdminRegisterRequest;
import com.pricelock.model.CustomerRegistrationRequest;
import com.pricelock.model.StoreOwnerRegisterRequest;
import com.pricelock.repository.UserInfoRepository;
import com.pricelock.repository.UserRepository;
import com.pricelock.service.RegistrationService;
import com.pricelock.table.User;
import com.pricelock.table.UserInfo;
import com.pricelock.util.EmailUtil;
import com.pricelock.util.ErrorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {
    private final ErrorUtil errorUtil;
    private final UserRepository userRepository;
    private final UserInfoRepository userInfoRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private EmailUtil emailUtil;

    @Override
    public void registerAdmin(AdminRegisterRequest adminRegisterRequest) {
        try {
            log.info("registerAdmin - start");
            User user = User.builder()
                    .username(adminRegisterRequest.getUsername())
                    .password(this.bCryptPasswordEncoder.encode(adminRegisterRequest.getPassword()))
                    .enabled(Boolean.TRUE)
                    .role(CommonConstants.ADMIN).build();
            user = userRepository.save(user);
            userInfoRepository.save(UserInfo.builder()
                    .user(user)
                    .firstName(adminRegisterRequest.getFirstName())
                    .lastName(adminRegisterRequest.getLastName())
                    .build());

            log.info("registerAdmin - end");
        } catch (Exception ex) {
            errorUtil.handleError(ex.getMessage());
        }

    }

    @Override
    public void registerStoreOwner(StoreOwnerRegisterRequest storeOwnerRegisterRequest) {
        try {
            log.info("registerStoreOwner - start");
            User user = User.builder()
                    .username(storeOwnerRegisterRequest.getUsername())
                    .password(this.bCryptPasswordEncoder.encode(storeOwnerRegisterRequest.getPassword()))
                    .enabled(Boolean.FALSE)
                    .role(CommonConstants.STORE_OWNER).build();
            user = userRepository.save(user);

            userInfoRepository.save(UserInfo.builder()
                    .user(user)
                    .storeName(storeOwnerRegisterRequest.getStoreName())
                    .email(storeOwnerRegisterRequest.getEmailAddress())
                    .phone(storeOwnerRegisterRequest.getPhoneNumber())
                    .province(storeOwnerRegisterRequest.getProvince())
                    .city(storeOwnerRegisterRequest.getCity())
                    .barangay(storeOwnerRegisterRequest.getBarangay())
                    .streetAddress(storeOwnerRegisterRequest.getStreetAddress())
                    .validId(convertToBlob(storeOwnerRegisterRequest.getValidId()))
                    .businessPermit(convertToBlob(storeOwnerRegisterRequest.getBarangayBusinessPermit()))
                    .dtiPermit(convertToBlob(storeOwnerRegisterRequest.getDtiRegistration()))
                    .mayorPermit(convertToBlob(storeOwnerRegisterRequest.getMayorPermit()))
                    .build());
            emailUtil.sendEmail(storeOwnerRegisterRequest.getEmailAddress(), "Hi " + storeOwnerRegisterRequest.getStoreName() +
                    "\n\n" +
                    "Please confirm your account by clicking this link http://192.168.51.246:8080/register/store/owner?"+user.getId() , "Price Lock | Account Confirmation");
            log.info("registerStoreOwner - end");
        } catch (Exception ex) {
            ex.printStackTrace();
            errorUtil.handleError(ex.getMessage());
        }
    }

    @Override
    public void registerCustomer(CustomerRegistrationRequest customerRegistrationRequest) {
        try {
            log.info("registerCustomer - start");
            User user = User.builder()
                    .username(customerRegistrationRequest.getUsername())
                    .password(this.bCryptPasswordEncoder.encode(customerRegistrationRequest.getPassword()))
                    .enabled(Boolean.TRUE)
                    .role(CommonConstants.CUSTOMER).build();
            user = userRepository.save(user);

            userInfoRepository.save(UserInfo.builder()
                    .user(user)
                    .email(customerRegistrationRequest.getEmailAddress())
                    .phone(customerRegistrationRequest.getPhoneNumber())
                    .build());
            emailUtil.sendEmail(customerRegistrationRequest.getEmailAddress(), "Hi " + customerRegistrationRequest.getUsername() +
                    "\n\n" +
                    "Welcome to pricelock." , "Price Lock | Welcome");
            log.info("registerCustomer - end");
        } catch (Exception ex) {
            ex.printStackTrace();
            errorUtil.handleError(ex.getMessage());
        }
    }

    @Override
    public void confirmRegistration(Long id) {
        User user =  userRepository.findById(id).orElse(null);
        if(Objects.nonNull(user) && !user.isEnabled()) {
            user.setEnabled(Boolean.TRUE);
            userRepository.save(user);
        } else {
            throw new APIException("Invalid url link.");
        }
    }

    private Blob convertToBlob(String base64) throws SQLException {
        return new SerialBlob(Base64.decodeBase64(base64));
    }

}
