package com.pricelock.service.impl;

import com.pricelock.exception.APIException;
import com.pricelock.repository.UserInfoRepository;
import com.pricelock.repository.UserRepository;
import com.pricelock.service.ForgotPasswordService;
import com.pricelock.table.User;
import com.pricelock.table.UserInfo;
import com.pricelock.util.EmailUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class ForgotPasswordServiceImpl implements ForgotPasswordService {
    private final UserInfoRepository userInfoRepository;
    private final UserRepository userRepository;
    private final EmailUtil emailUtil;

    @Override
    public void forgotPasswordStoreOwner(String emailAddress) {
        log.info("start - forgotPasswordStoreOwner");
        UserInfo userInfo = userInfoRepository.findUserInfoByEmail(emailAddress);
        if (Objects.nonNull(userInfo)) {
            User user = userInfo.getUser();
            user.setReset(Boolean.TRUE);
            userRepository.save(user);
            emailUtil.sendEmail(emailAddress, "Hi " + userInfo.getStoreName() +
                    "\n\n" +
                    "Please reset your password by clicking this link http://192.168.51.246:8080/reset/password/store/owner?id="+user.getId() , "Price Lock | Forgot Password");
        } else {
            throw new APIException("Email address does not exists.");
        }

        log.info("end - forgotPasswordStoreOwner");
    }
}
