package com.pricelock.service.impl;

import com.pricelock.model.OrderResponse;
import com.pricelock.repository.ItemOrderRepository;
import com.pricelock.service.DashboardService;
import com.pricelock.table.ItemOrder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class DashboardServiceImpl implements DashboardService {
    private final ItemOrderRepository itemOrderRepository;
    @Override
    public List<OrderResponse> retrieveAllOrder(Long id) {
        log.info("start - retrieveAllOrder");
        List<ItemOrder> itemOrderList = itemOrderRepository.findAllItemOrderByReady(Boolean.FALSE)
                .orElse(Collections.emptyList());
        ItemOrder itemOrderMapper = new ItemOrder();
        log.info("end - retrieveAllOrder");
        return itemOrderMapper.itemOrderToOrderResponseAll(itemOrderList);
    }
}
