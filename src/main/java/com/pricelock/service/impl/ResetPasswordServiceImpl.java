package com.pricelock.service.impl;

import com.pricelock.exception.APIException;
import com.pricelock.model.ResetPasswordRequest;
import com.pricelock.repository.UserRepository;
import com.pricelock.service.ResetPasswordService;
import com.pricelock.table.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class ResetPasswordServiceImpl implements ResetPasswordService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Override
    public void resetPassword(ResetPasswordRequest resetPasswordRequest) {
        try {
            log.info("start - resetPassword");
            User user = userRepository.findById(resetPasswordRequest.getId()).orElse(null);
            if (Objects.nonNull(user) && user.isReset()) {
                user.setPassword(this.bCryptPasswordEncoder.encode(resetPasswordRequest.getPassword()));
                user.setReset(Boolean.FALSE);
                userRepository.save(user);
            } else {
                throw new APIException("Unexpected response, cannot reset password.");
            }
            log.info("end - resetPassword");
        } catch (Exception ex) {
            throw new APIException("Error encountered during password reset.");
        }
    }
}
