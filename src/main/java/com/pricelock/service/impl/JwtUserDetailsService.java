package com.pricelock.service.impl;

import com.pricelock.model.MyUserDetails;
import com.pricelock.repository.UserRepository;
import com.pricelock.table.User;
import com.pricelock.table.UserInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Getter
    @Setter
    private String fullName;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username);
        if (Objects.nonNull(user)) {
            UserInfo userInfo = user.getUserInfo();
            setFullName(userInfo.getFullName());
            return new MyUserDetails(user);
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }

}
