package com.pricelock.service.impl;

import com.pricelock.model.BarangayRequest;
import com.pricelock.model.CityRequest;
import com.pricelock.model.Location;
import com.pricelock.repository.BarangayRepository;
import com.pricelock.repository.CityRepository;
import com.pricelock.repository.ProvinceRepository;
import com.pricelock.service.LocationService;
import com.pricelock.table.Barangay;
import com.pricelock.table.City;
import com.pricelock.table.Province;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class LocationServiceImpl implements LocationService {
    private final ProvinceRepository provinceRepository;
    private final CityRepository cityRepository;
    private final BarangayRepository barangayRepository;

    @Override
    public List<Location> getAllProvince() {
        log.info("start - getAllProvince");
        List<Province> provinces = provinceRepository.findAll();
        Province provinceMapper = new Province();
        log.info("end - getAllProvince");
        return provinceMapper.provinceToLocationAll(provinces);
    }

    @Override
    public List<Location> getAllCityByProvince(CityRequest cityRequest) {
        log.info("start - getAllCityByProvince");
        if(Objects.nonNull(cityRequest)) {
            List<City> cities = cityRepository.findAllByProvCode(cityRequest.getProvinceCode());
            City cityMapper = new City();
            log.info("end - getAllCityByProvince");
            return cityMapper.cityToLocationAll(cities);
        }
        log.info("end - getAllCityByProvince");
        return Collections.emptyList();
    }

    @Override
    public List<Location> getAllBarangayByCity(BarangayRequest barangayRequest) {
        log.info("start - getAllBarangayByCity");
        if(Objects.nonNull(barangayRequest)) {
            List<Barangay> barangays = barangayRepository.findAllByCityMunCode(barangayRequest.getCityCode());
            Barangay barangayMapper = new Barangay();
            log.info("end - getAllBarangayByCity");
            return barangayMapper.barangayToLocationAll(barangays);
        }
        log.info("end - getAllBarangayByCity");
        return Collections.emptyList();
    }
}
