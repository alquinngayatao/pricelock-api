package com.pricelock.service;

import com.pricelock.model.AddItemRequest;
import com.pricelock.model.AddReviewRequest;
import com.pricelock.model.CheckoutRequest;
import com.pricelock.model.OrderRequest;
import com.pricelock.model.ReviewResponse;
import com.pricelock.model.StoreResponse;

import java.sql.SQLException;
import java.util.List;

public interface ItemService {
    void addItem(AddItemRequest addItemRequest);
    void addReview(AddReviewRequest addReviewRequest);
    List<ReviewResponse> retrieveAllReview(Long itemId);
    void order(OrderRequest orderRequest);
    void checkout(CheckoutRequest checkoutRequest);
    List<StoreResponse> retrieveAllStore();
}
