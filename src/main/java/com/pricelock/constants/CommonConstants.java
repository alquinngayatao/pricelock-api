package com.pricelock.constants;

public class CommonConstants {
    public static final String ADMIN = "ADMIN";
    public static final String STORE_OWNER = "STORE_OWNER";
    public static final String CUSTOMER = "CUSTOMER";
}
