package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class StoreOwnerRegisterRequest implements Serializable {
    private String emailAddress;
    private String username;
    private String password;
    private String phoneNumber;
    private String storeName;
    private String province;
    private String city;
    private String barangay;
    private String streetAddress;
    private String validId;
    private String barangayBusinessPermit;
    private String dtiRegistration;
    private String mayorPermit;
}
