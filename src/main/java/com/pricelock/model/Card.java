package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;

@Data
public class Card implements Serializable {
    private BigInteger cardNumber;
    private String firstName;
    private String lastName;
    private String middleName;
    private Integer securityCode;
    private String expiration;
}
