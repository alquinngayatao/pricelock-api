package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class AddReviewRequest implements Serializable {
    private Long itemId;
    private String name;
    private String feedback;
    private Integer rate;
}
