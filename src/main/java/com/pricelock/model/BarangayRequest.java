package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class BarangayRequest implements Serializable {
    private String cityCode;
}
