package com.pricelock.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class PersonalDetails implements Serializable {
    private String fullName;
    private JwtResponse jwtResponse;
}
