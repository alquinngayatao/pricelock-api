package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdminRegisterRequest implements Serializable {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
}
