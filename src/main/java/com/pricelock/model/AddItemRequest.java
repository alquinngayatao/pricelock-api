package com.pricelock.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.math.BigInteger;

@Data
@Slf4j
public class AddItemRequest implements Serializable {
    private String name;
    private BigInteger price;
    private String description;
    private String image;
    private Long id;
}
