package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResetPasswordRequest implements Serializable {
    private Long id;
    private String password;
}
