package com.pricelock.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemResponse implements Serializable {
    private Long id;
    private String name;
    private String description;
    private BigInteger price;
    private String image;
    private Long storeOwnerId;
    private LocalDate dTimeCreated;
}
