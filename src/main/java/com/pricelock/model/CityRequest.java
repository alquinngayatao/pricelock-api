package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CityRequest implements Serializable {
    private String provinceCode;
}
