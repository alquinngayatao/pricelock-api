package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CustomerRegistrationRequest implements Serializable {
    private String emailAddress;
    private String username;
    private String password;
    private String phoneNumber;
}
