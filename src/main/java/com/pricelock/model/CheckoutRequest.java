package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CheckoutRequest implements Serializable {
    private Long id;
}
