package com.pricelock.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ItemDetail implements Serializable {
    private Long id;
    private Integer quantity;
}
