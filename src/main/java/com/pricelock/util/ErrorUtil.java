package com.pricelock.util;

import com.pricelock.exception.APIException;
import org.springframework.stereotype.Component;

@Component
public class ErrorUtil {

    public void handleError(String errorMessage) {
        throw new APIException(errorMessage);
    }
}
