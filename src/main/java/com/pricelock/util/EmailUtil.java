package com.pricelock.util;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class EmailUtil {

    public void sendEmail(String receiverEmailAddress, String emailMessage, String subject) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("pricelock2022@gmail.com");
        message.setTo(receiverEmailAddress);
        message.setSubject(subject);
        message.setText(emailMessage);
        getMainSender().send(message);
    }

    private JavaMailSender getMainSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("pricelock2022@gmail.com");
        mailSender.setPassword("wynpsqjisaehceye");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}
