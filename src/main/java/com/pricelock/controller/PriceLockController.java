package com.pricelock.controller;

import com.pricelock.model.AdminRegisterRequest;
import com.pricelock.model.BarangayRequest;
import com.pricelock.model.CityRequest;
import com.pricelock.model.CommonResponse;
import com.pricelock.model.CustomerRegistrationRequest;
import com.pricelock.model.Location;
import com.pricelock.model.ResetPasswordRequest;
import com.pricelock.model.StoreOwnerRegisterRequest;
import com.pricelock.service.DashboardService;
import com.pricelock.service.ForgotPasswordService;
import com.pricelock.service.LocationService;
import com.pricelock.service.RegistrationService;
import com.pricelock.service.ResetPasswordService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class PriceLockController {
    private final RegistrationService registrationService;
    private final ForgotPasswordService forgotPasswordService;
    private final LocationService locationService;
    private final ResetPasswordService resetPasswordService;
    private final DashboardService dashboardService;

    @Transactional
    @RequestMapping(value = "/register/admin", method = RequestMethod.POST)
    public ResponseEntity<?> createAdmin(@RequestBody AdminRegisterRequest adminRegisterRequest) {
        registrationService.registerAdmin(adminRegisterRequest);
        return ResponseEntity.ok().build();
    }

    @Transactional
    @RequestMapping(value = "/register/store/owner", method = RequestMethod.POST)
    public ResponseEntity<?> createStoreOwner(@RequestBody StoreOwnerRegisterRequest storeOwnerRegisterRequest) {
        registrationService.registerStoreOwner(storeOwnerRegisterRequest);
        return ResponseEntity.ok().build();
    }

    @Transactional
    @RequestMapping(value = "/register/customer", method = RequestMethod.POST)
    public ResponseEntity<?> createCustomer(@RequestBody CustomerRegistrationRequest customerRegistrationRequest) {
        registrationService.registerCustomer(customerRegistrationRequest);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/register/store/owner/confirm", method = RequestMethod.GET)
    public ResponseEntity<?> confirmRegistrationStoreOwner(@RequestParam Long id) {
        registrationService.confirmRegistration(id);
        return ResponseEntity.ok(CommonResponse.builder()
                .message("Account successfully activated.").build());
    }

    @RequestMapping(value = "/forgot/password/store/owner", method = RequestMethod.GET)
    public ResponseEntity<?> forgotPasswordStoreOwner(@RequestParam String emailAddress) {
        forgotPasswordService.forgotPasswordStoreOwner(emailAddress);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/dashboard/store/owner/order", method = RequestMethod.GET)
    public ResponseEntity<?> retrieveDashboard(@RequestParam Long id) {
        return ResponseEntity.ok(dashboardService.retrieveAllOrder(id));
    }

    @Transactional
    @RequestMapping(value = "/reset/password/store/owner", method = RequestMethod.POST)
    public ResponseEntity<?> resetPasswordStoreOwner(@RequestBody ResetPasswordRequest resetPasswordRequest) {
        resetPasswordService.resetPassword(resetPasswordRequest);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/location/province/all", method = RequestMethod.GET)
    public ResponseEntity<List<Location>> retrieveAllProvince() {
        return ResponseEntity.ok(locationService.getAllProvince());
    }

    @RequestMapping(value = "/location/city", method = RequestMethod.POST)
    public ResponseEntity<List<Location>> retrieveCityByProvince(@RequestBody CityRequest cityRequest) {
        return ResponseEntity.ok(locationService.getAllCityByProvince(cityRequest));
    }

    @RequestMapping(value = "/location/barangay", method = RequestMethod.POST)
    public ResponseEntity<List<Location>> retrieveCityByProvince(@RequestBody BarangayRequest barangayRequest) {
        return ResponseEntity.ok(locationService.getAllBarangayByCity(barangayRequest));
    }
}
