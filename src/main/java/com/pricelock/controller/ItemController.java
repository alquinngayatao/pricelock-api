package com.pricelock.controller;

import com.pricelock.model.AddItemRequest;
import com.pricelock.model.AddReviewRequest;
import com.pricelock.model.CheckoutRequest;
import com.pricelock.model.OrderRequest;
import com.pricelock.model.ReviewResponse;
import com.pricelock.model.StoreResponse;
import com.pricelock.service.ItemService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class ItemController {
    private final ItemService itemService;

    @Transactional
    @RequestMapping(value = "/item/store/owner/add", method = RequestMethod.POST)
    public ResponseEntity<?> retrieveDashboard(@RequestBody AddItemRequest addItemRequest) {
        itemService.addItem(addItemRequest);
        return ResponseEntity.ok().build();
    }

    @Transactional
    @RequestMapping(value = "/item/customer/review/add", method = RequestMethod.POST)
    public ResponseEntity<?> retrieveDashboard(@RequestBody AddReviewRequest addReviewRequest) {
        itemService.addReview(addReviewRequest);
        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/item/customer/review/retrieve", method = RequestMethod.GET)
    public ResponseEntity<List<ReviewResponse>> retrieveDashboard(@RequestParam Long itemId) {
        return ResponseEntity.ok(itemService.retrieveAllReview(itemId));
    }

    @Transactional
    @RequestMapping(value = "/item/customer/order", method = RequestMethod.POST)
    public ResponseEntity<List<ReviewResponse>> retrieveDashboard(@RequestBody OrderRequest orderRequest) {
        itemService.order(orderRequest);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/item/customer/checkout", method = RequestMethod.POST)
    public ResponseEntity<List<ReviewResponse>> checkout(@RequestBody CheckoutRequest checkoutRequest) {
        itemService.checkout(checkoutRequest);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/store/customer/retrieve", method = RequestMethod.GET)
    public ResponseEntity<List<StoreResponse>> retrieveAllStores() {
        return ResponseEntity.ok(itemService.retrieveAllStore());
    }
}
