package com.pricelock.table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pricelock.model.OrderResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder(toBuilder = true)
@Table(name = "item_order")
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class ItemOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(mappedBy = "itemOrder")
    private Set<CustomerOrder> customerOrders;
    private Boolean ready;
    private Boolean delivered;
    @Column(name = "ID_CUSTOMER")
    private Long customerId;
    @CreatedDate
    @Column(name = "DTIME_CREATED")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate dTimeCreated;

    public List<OrderResponse> itemOrderToOrderResponseAll(List<ItemOrder> itemOrders) {
        if (CollectionUtils.isEmpty(itemOrders)) {
            return Collections.emptyList();
        }

        List<OrderResponse> orderList = new ArrayList<>();
        for (ItemOrder itemOrder : itemOrders) {
            for (CustomerOrder customerOrder : itemOrder.getCustomerOrders()) {
                orderList.add(itemOrderToOrderResponse(customerOrder));
            }
        }
        return orderList;
    }

    private OrderResponse itemOrderToOrderResponse(CustomerOrder customerOrder) {
        return OrderResponse.builder()
                .itemName(customerOrder.getItem().getName())
                .itemQuantity("x " + customerOrder.getQuantity())
                .price("₱ " + customerOrder.getItem().getPrice())
                .build();
    }
}
