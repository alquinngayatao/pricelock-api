package com.pricelock.table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pricelock.model.ItemResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Builder
@Table(name = "item")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "customerOrder")
@EntityListeners(AuditingEntityListener.class)
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private BigInteger price;
    private Blob image;
    @OneToOne(mappedBy = "item", fetch = FetchType.LAZY)
    private CustomerOrder customerOrder;
    @Column(name = "ID_STORE_OWNER")
    private Long storeOwnerId;
    @CreatedDate
    @Column(name = "DTIME_CREATED")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate dTimeCreated;


    public List<ItemResponse> itemAllToResponse(List<Item> items) throws SQLException {
        if (CollectionUtils.isEmpty(items)) {
            return Collections.emptyList();
        }

        List<ItemResponse> itemResponses = new ArrayList<>();
        for (Item item : items) {
            itemResponses.add(itemToResponse(item));
        }
        return itemResponses;
    }

    public ItemResponse itemToResponse(Item item) throws SQLException {
        if (Objects.isNull(item)) {
            return null;
        }

        return ItemResponse.builder()
                .id(item.getId())
                .description(item.getDescription())
                .dTimeCreated(item.getDTimeCreated())
                .price(item.getPrice())
                .storeOwnerId(item.getStoreOwnerId())
                .image(convertToBase64(item.getImage()))
                .build();
    }

    private String convertToBase64(Blob blob) throws SQLException {
        byte[] imageByte = blob.getBytes(1, (int) blob.length());
        return new String(imageByte, StandardCharsets.UTF_8);
    }
}
