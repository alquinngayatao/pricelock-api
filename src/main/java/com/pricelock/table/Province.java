package com.pricelock.table;

import com.pricelock.model.Location;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.text.WordUtils;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Builder
@Table(name = "refprovince")
@NoArgsConstructor
@AllArgsConstructor
public class Province {

    @Id
    private Long id;
    @Column(name = "CODE_PSGC")
    private String psgcCode;
    @Column(name = "DESC_PROV")
    private String provDesc;
    @Column(name = "CODE_REG")
    private String regCode;
    @Column(name = "CODE_PROV")
    private String provCode;

    public List<Location> provinceToLocationAll(List<Province> provinces){
        if(CollectionUtils.isEmpty(provinces)) {
            return Collections.emptyList();
        }

        List<Location> locations = new ArrayList<>();
        for(Province province: provinces) {
            locations.add(provinceToLocation(province));
        }
        return locations;
    }

    public Location provinceToLocation(Province province) {
        if(Objects.isNull(province)) {
            return null;
        }

        return Location.builder()
                .code(province.provCode)
                .label(WordUtils.capitalizeFully(province.provDesc))
                .build();
    }
}
