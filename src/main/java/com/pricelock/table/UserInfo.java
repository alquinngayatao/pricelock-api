package com.pricelock.table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Blob;

@Data
@Entity
@Builder
@Table(name = "user_info")
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String storeName;
    @Column(unique=true)
    private String email;
    private String phone;
    private String province;
    private String city;
    private String barangay;
    private String streetAddress;
    private Blob validId;
    private Blob businessPermit;
    private Blob dtiPermit;
    private Blob mayorPermit;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_USERS", referencedColumnName = "ID")
    private User user;

    public String getFullName(){
       return firstName + " " + lastName;
    }
}
