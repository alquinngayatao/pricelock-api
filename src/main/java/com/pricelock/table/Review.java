package com.pricelock.table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pricelock.model.ReviewResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Builder
@Table(name = "review")
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String feedback;
    private Integer rate;
    @Column(name = "ID_ITEM")
    private Long itemId;
    @CreatedDate
    @Column(name = "DTIME_CREATED")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    private LocalDate dTimeCreated;

    public List<ReviewResponse> reviewToResponseAll(List<Review> reviews) {
        if (CollectionUtils.isEmpty(reviews)) {
            return Collections.emptyList();
        }

        List<ReviewResponse> locations = new ArrayList<>();
        for (Review review : reviews) {
            locations.add(reviewToResponse(review));
        }
        return locations;
    }

    public ReviewResponse reviewToResponse(Review review) {
        if (Objects.isNull(review)) {
            return null;
        }

        return ReviewResponse.builder()
                .rate(review.getRate())
                .name(review.getName())
                .feedback(review.getFeedback())
                .build();
    }
}
