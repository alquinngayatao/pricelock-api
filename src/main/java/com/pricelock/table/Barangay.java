package com.pricelock.table;

import com.pricelock.model.Location;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.text.WordUtils;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Builder
@Table(name = "refbrgy")
@NoArgsConstructor
@AllArgsConstructor
public class Barangay {
    @Id
    private Long id;
    @Column(name = "CODE_BRGY")
    private String brgyCode;
    @Column(name = "DESC_BRGY")
    private String brgyDesc;
    @Column(name = "CODE_REG")
    private String regCode;
    @Column(name = "CODE_PROV")
    private String provCode;
    @Column(name = "CODE_CITY_MUN")
    private String cityMunCode;

    public List<Location> barangayToLocationAll(List<Barangay> barangays){
        if(CollectionUtils.isEmpty(barangays)) {
            return Collections.emptyList();
        }

        List<Location> locations = new ArrayList<>();
        for(Barangay barangay: barangays) {
            locations.add(barangayToLocation(barangay));
        }
        return locations;
    }

    public Location barangayToLocation(Barangay barangay) {
        if(Objects.isNull(barangay)) {
            return null;
        }

        return Location.builder()
                .code(barangay.brgyCode)
                .label(WordUtils.capitalizeFully(barangay.brgyDesc))
                .build();
    }
}
