package com.pricelock.table;

import com.pricelock.model.Location;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.text.WordUtils;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Builder
@Table(name = "refcitymun")
@NoArgsConstructor
@AllArgsConstructor
public class City {
    @Id
    private Long id;
    @Column(name = "CODE_PSGC")
    private String psgcCode;
    @Column(name = "DESC_CITY_MUN")
    private String cityMunDesc;
    @Column(name = "DESC_REG")
    private String regDesc;
    @Column(name = "CODE_PROV")
    private String provCode;
    @Column(name = "CODE_CITY_MUN")
    private String cityMunCode;

    public List<Location> cityToLocationAll(List<City> cities){
        if(CollectionUtils.isEmpty(cities)) {
            return Collections.emptyList();
        }

        List<Location> locations = new ArrayList<>();
        for(City city: cities) {
            locations.add(cityToLocation(city));
        }
        return locations;
    }

    public Location cityToLocation(City city) {
        if(Objects.isNull(city)) {
            return null;
        }

        return Location.builder()
                .code(city.cityMunCode)
                .label(WordUtils.capitalizeFully(city.cityMunDesc))
                .build();
    }
}
