package com.pricelock.exception;

import com.pricelock.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = APIException.class)
    protected ResponseEntity<Object> handleAPIException(RuntimeException ex) {
        return ResponseEntity.internalServerError()
                .body(ErrorResponse.builder()
                        .message(ex.getMessage())
                        .build());
    }
}
