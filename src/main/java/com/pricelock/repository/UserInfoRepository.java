package com.pricelock.repository;

import com.pricelock.table.UserInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoRepository extends CrudRepository<UserInfo, Long> {
    UserInfo findUserInfoByEmail(String emailAddress);
}
