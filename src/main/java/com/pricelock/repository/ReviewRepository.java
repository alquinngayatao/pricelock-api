package com.pricelock.repository;

import com.pricelock.table.Review;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {
    List<Review> findAllReviewById(Long id);
}
