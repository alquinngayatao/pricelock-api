package com.pricelock.repository;

import com.pricelock.table.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
     User findUserByUsername(String username);
     List<User> findAllUserByRole(String role);
}
