package com.pricelock.repository;

import com.pricelock.table.ItemOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ItemOrderRepository extends JpaRepository<ItemOrder, Long> {
    Optional<List<ItemOrder>> findAllItemOrderByReady(Boolean ready);
    Optional<ItemOrder> findItemOrderByCustomerId(Long id);
}
