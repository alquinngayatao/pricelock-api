package com.pricelock.repository;

import com.pricelock.table.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {
    List<Item> findAllItemByStoreOwnerId(Long id);
}
