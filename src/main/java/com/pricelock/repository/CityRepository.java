package com.pricelock.repository;

import com.pricelock.table.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {
    List<City> findAllByProvCode(String provinceCode);
}
