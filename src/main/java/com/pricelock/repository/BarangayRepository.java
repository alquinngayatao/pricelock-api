package com.pricelock.repository;

import com.pricelock.table.Barangay;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BarangayRepository extends JpaRepository<Barangay, Long> {
    List<Barangay> findAllByCityMunCode(String cityCode);
}
